# Live coding exercise

Welcome! Please take 15 minutes and get yourself set up.

### Stuff to mention beforehand
* The goal would be to implement features in PhoneNumber class so that tests pass
* It is not important how far you get during the time
* Use whatever means and tools he needs: Google, Stackoverflow, whatever he would use at work
* Be as verbal as possible with your thought process to make it “visible” what you think – just talk out loud if possible
* Modify whatever needed – no code is sacred, everything is yours to change

### For the first milestone candidate has 10-15 minutes
* Time is started once he has project set up
* After initial 10-15min give brief feedback about good and bad
* If you see only bad and feel the 10min was waste of time, then just say you got all the information you needed and it seems that there is no good match between the required skills and candidate
* If you got interested, ask him to continue further 30 minutes.

### Stuff to observe
* How does he understand existing code (navigating 2 classes should not pose difficulties to any developer)
* No TDD experience usually start implementing without running tests
* How he names variables, methods
* Does he read all the code and try to make conclusions
* Does he try to improve the existing code
* How time pressure affects him
* Problem solving skill – what he does, when stuck
* How fluent he is in IDE, does he look for shortcuts
* Does he know the language (by heart) - knows what and where to look in JavaDoc or googles problems hoping to get straight solutions