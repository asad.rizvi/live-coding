package main

import "testing"

func Test_test1(t *testing.T) {
	ph := createPhoneNumber("+1(858)775-2868")

	if got := ph.GetOriginalText(); got != "+1(858)775-2868" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetStrippedNumber(); got != "+18587752868" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetValueAsNorthAmerican(); got != "(858)775-2868" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetValueAsInternational(); got != "+1.858.775.2868" {
		t.Errorf("failed, got: %s", got)
	}
}

func Test_test2(t *testing.T) {
	ph := createPhoneNumber("+1(858)775-2868x123")

	if got := ph.GetOriginalText(); got != "+1(858)775-2868x123" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetStrippedNumber(); got != "+18587752868x123" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetValueAsNorthAmerican(); got != "(858)775-2868x123" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetValueAsInternational(); got != "+1(858)775-2868x123" {
		t.Errorf("failed, got: %s", got)
	}
}

func Test_test3(t *testing.T) {
	ph := createPhoneNumber("+598.123.4567x858")

	if got := ph.GetOriginalText(); got != "+598.123.4567x858" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetStrippedNumber(); got != "+5981234567x858" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetValueAsNorthAmerican(); got != "" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetValueAsInternational(); got != "+598.123.456.7x858" {
		t.Errorf("failed, got: %s", got)
	}
}

func Test_test4(t *testing.T) {
	ph := createPhoneNumber("+27 1234 5678 ext 4")

	if got := ph.GetOriginalText(); got != "+27 1234 5678 ext 4" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetStrippedNumber(); got != "+2712345678x4" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetValueAsNorthAmerican(); got != "" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetValueAsInternational(); got != "+27 1234 5678 ext 4" {
		t.Errorf("failed, got: %s", got)
	}
}

func Test_test5(t *testing.T) {
	ph := createPhoneNumber("858-775-2868")

	if got := ph.GetOriginalText(); got != "858-775-2868" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetStrippedNumber(); got != "+18587752868" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetValueAsNorthAmerican(); got != "(858)775-2868" {
		t.Errorf("failed, got: %s", got)
	}

	if got := ph.GetValueAsInternational(); got != "+1.858.775.2868" {
		t.Errorf("failed, got: %s", got)
	}
}
